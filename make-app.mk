app:
	docker-compose up api mongodb

run-front:
	docker-compose up frontend

app-build:
	docker-compose build

app-bash:
	docker-compose run frontend sh

development-setup-env:
	ansible-playbook ansible/development.yml -i ansible/development

app-setup: development-setup-env app-build app-mongo app-mongo-restore app-down

app-mongo:
	docker-compose up -d mongodb

app-mongo-restore:
	docker-compose exec mongodb bash -c "mongorestore /backup --host localhost:27017"

app-mongo-dump:
	docker-compose exec mongodb bash -c "mongodump -o /backup --host localhost:27017"

app-down:
	docker-compose down

production-setup:
	ansible-playbook ansible/site.yml -i ansible/production -u ubuntu -vv

production-deploy:
	ansible-playbook ansible/deploy.yml -i ansible/production -u ubuntu -vv

production-deploy-app:
	ansible-playbook ansible/deploy.yml -i ansible/production -u ubuntu -vv -t app
