import React from 'react';
import { Container, Header, List } from 'semantic-ui-react';
import classNames from 'helpers/classNames';
import styles from './Books.css';
import { Card, Icon, Image, Label } from 'semantic-ui-react'

const cn = classNames(styles);

const BookLink = ({ children }) => (
    <div style={{marginRight: 10, display: 'inline'}}>
        <Label style={{fontSize: 15, marginTop: 10}}>
            { children }
        </Label>
    </div>
)

const CardExampleCard = ({ poster, title, author, genre, year, pages, links = [] }) => (
    <Card style={{
        margin: '10px !important'
    }}>
        <Image src={poster} style={{height: 360}}/>
        <Card.Content>
            <Card.Header>{title}</Card.Header>
            <Card.Meta>
        <span className='date'>
            {author}
        </span>
            </Card.Meta>
            <Card.Description>
                Жанр: { genre }.
                { !!year &&
                    <span><br/>
                    Бастырылу елы: {year}.</span>
                }
                <br/>
                Күләме: {pages} бит.
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            {
                links.map(({ link, type}) => (
                    <BookLink>
                        <a href={link}>
                            <Icon name='external' />
                            { type}
                        </a>
                    </BookLink>
                ))
            }
        </Card.Content>
    </Card>
)


const data = [
    {
        poster: 'http://51.15.82.57:8080/books/durtkyz.jpg',
        title: 'Исемем минем Дүрткүз',
        author: 'Роберт Батуллин',
        genre: 'повесть, балаларга, маҗаралар',
        year: '1966',
        pages: 40,
        links: [
            {link: 'http://51.15.82.57:8080/books/durtkyz.txt', type: 'txt'},
            {link: 'http://51.15.82.57:8080/books/durtkyz.fb2', type: 'fb2'},
            {link: 'http://51.15.82.57:8080/books/durtkyz.docx', type: 'docx'},
        ]
    }, {
        poster: 'http://51.15.82.57:8080/books/rab/Robi_jpg.jpg',
        title: 'Робинзон Крузо',
        author: 'Роберт Батуллин',
        genre: 'роман, балаларга, маҗаралар',
        year: '1719',
        pages: 220,
        links: [
            {link: 'http://51.15.82.57:8080/books/rab/Robinzon.txt', type: 'txt'},
            {link: 'http://51.15.82.57:8080/books/rab/Robinzon.fb2', type: 'fb2'},
            {link: 'http://51.15.82.57:8080/books/rab/Robinzon.epub', type: 'epub'},
            {link: 'http://51.15.82.57:8080/books/rab/Robinzon.docx', type: 'docx'},
        ]
    }, {
        poster: 'http://51.15.82.57:8080/books/basker.jpg',
        title: 'Баскервильләр эте',
        author: 'Артур Конан Дойл',
        genre: 'детектив, балаларга',
        year: '1901',
        pages: 170,
        links: [
            {link: 'http://51.15.82.57:8080/books/basker.txt', type: 'txt'},
            {link: 'http://51.15.82.57:8080/books/basker.fb2', type: 'fb2'},
            {link: 'http://51.15.82.57:8080/books/basker.epub', type: 'epub'},
            {link: 'http://51.15.82.57:8080/books/basker.docx', type: 'docx'}
        ]
    }, {
        poster: 'http://51.15.82.57:8080/books/books/alish/raseme.jpg',
        title: 'Абдулла Алиш әкиятләре',
        author: 'Абдулла Алиш',
        genre: 'анималистик хикәяләр, балаларга',
        year: false,
        pages: 56,
        links: [
            {link: 'http://51.15.82.57:8080/books/books/alish/akiyatlar_a_alis.epub', type: 'epub'},
            {link: 'http://51.15.82.57:8080/books/books/alish/akiyatlar_a_alis.fb2', type: 'fb2'},
            {link: 'http://51.15.82.57:8080/books/books/alish/akiyatlar_a_alis.mobi', type: 'mobi'},
            {link: 'http://51.15.82.57:8080/books/books/alish/akiyatlar_a_alis.pdf', type: 'pdf'},
            {link: 'http://51.15.82.57:8080/books/books/alish/akiyatlar_a_alis.txt', type: 'txt'},
        ]
    }, {
        poster: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.jpg',
        title: 'Нәни принц',
        author: 'Антуан де Сент-Экзюпери',
        genre: 'Аллегорик әкият-повесть, балаларга',
        year: '1943',
        pages: 57,
        links: [
            {link: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.epub', type: 'epub'},
            {link: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.fb2', type: 'fb2'},
            {link: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.mobi', type: 'mobi'},
            {link: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.txt', type: 'txt'},
            {link: 'http://51.15.82.57:8080/books/books/prin/nani_prins_a_de_s_ekzuperi.pdf', type: 'pdf'}
        ]
    }, {
        poster: 'http://51.15.82.57:8080/books/books/Chaptar/Captar_Jack_E_S_Thompson.jpg',
        title: 'Чаптар Җәк',
        author: 'Эрнест Сетон Томпсон',
        genre: 'анималистик хикәя, балаларга',
        year: false,
        pages: 38,
        links: [
            {link: 'http://51.15.82.57:8080/books/books/Chaptar/Captar_Jack_E_S_Thompson.txt', type: 'txt'},
            {link: 'http://51.15.82.57:8080/books/books/Chaptar/Captar_Jack_E_S_Thompson.pdf', type: 'pdf'},
            {link: 'http://51.15.82.57:8080/books/books/Chaptar/Captar_Jack_E_S_Thompson.epub', type: 'epub'},
            {link: 'http://51.15.82.57:8080/books/books/Chaptar/Captar_Jack_E_S_Thompson.mobi', type: 'mobi'},
        ]
    }
];

class Books extends React.Component {
	render() {
	    return (
            <Container main>
                <br/>
                <Header as='h1'>Китаплар: </Header>
                <div style={{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-around'
                }}>
                    {
                        data.reverse().map((props) => <CardExampleCard {...props}/>)
                    }
                </div>

				<br/>
				{
					global.window && global.innerWidth > 520 &&
					<iframe frameBorder="0" allowTransparency="true" scrolling="no" src="https://money.yandex.ru/embed/donate.xml?account=410014304147436&quickpay=donate&payment-type-choice=on&default-sum=100&targets=%D1%80%D0%B0%D0%B7%D0%B2%D0%B8%D1%82%D0%B8%D0%B5+%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0&project-name=%D0%91%D0%B5%D0%B7%D0%BD%D0%B5%D2%A3+%D1%82%D3%99%D2%97%D1%80%D0%B8%D0%B1%D3%99&project-site=bts.tatar&button-text=05&successURL=" width="422" height="92"></iframe>

				}
			</Container>
		)
	}
}

export default Books;
