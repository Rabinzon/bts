import React from 'react';
import Express from 'express';
import { match, RouterContext } from 'react-router';
import routes from './routes.jsx';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducer from './redux/modules/reducer';
import { renderToString } from 'react-dom/server';
import getHtml from 'helpers/getHtml';
const request = require("request");

const env = process.env || {}
const fetch = (url, res) => {
    request.get(url, (error, response, body) => {
    	if (error) {
            console.log(url, error);
            res.sendStatus(502)
            return
		}
        res.send(JSON.parse(body || '[]'));
    });
}

const handleRender = (req, res) => {
	const store = createStore(reducer);
	match({ routes: routes(), location: req.url }, (err, redirect, props) => {
		const html = renderToString(
			<Provider store={store}>
				<RouterContext {...props}/>
			</Provider>
		);
		const preloadedState = store.getState();
		res.send(getHtml(html, preloadedState));
	});
};

const app = Express();
app.use('/static', Express.static(__dirname + '/static'));

app.get('*', handleRender)
app.post('/', function (req, res) {
    console.log(env.API || 'localhost:1323');
    fetch('http://' + (env.API || 'localhost:1323'), res)
});

app.post('/movie/:id', function (req, res) {
    fetch('http://' + (env.API || 'localhost:1323') + '/movie/'+ req.params.id, res)
});

app.get('/files/:name', function (req, res) {
    fetch('http://51.15.82.57:8080/'+ req.params.name, res)
});

app.listen(env.APP_PORT || 3000, (err) => {
	if (err) {
		console.error(err);
	}
	console.info('==> 💻  Open http://%s:%s in a browser to view the app.', 'localhost', env.APP_PORT || 3000);
});
