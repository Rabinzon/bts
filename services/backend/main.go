package main

import (
	"fmt"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"net/http"
)

type Movie struct {
	Title      string
	Text       string
	Img        string
	Href       string
	Video      string
	Time       string
	Year       string
	Genres     string
	Tag        string
	Date       string
	Voice      string
	Name       string
	Editing    string
	Translator string
}

type MovieResponse struct {
	ID         bson.ObjectId `bson:"_id,omitempty"`
	Title      string        `json:"title"`
	Text       string        `json:"text"`
	Img        string        `json:"img"`
	Href       string        `json:"href"`
	Video      string        `json:"video"`
	Time       string        `json:"time"`
	Year       string        `json:"year"`
	Genres     string        `json:"genres"`
	Tag        string        `json:"tag"`
	Date       string        `json:"date"`
	Voice      string        `json:"voice"`
	Editing    string        `json:"editing"`
	Translator string        `json:"translator"`
	Name       string        `json:"name"`
}

type User struct {
	Text string `json:"text"`
}

const (
	Movie_Coll = "Movie"
	DB_Name    = "bts"
	DB_Path    = "mongodb:27017"
	Api_Port   = ":1323"
)

func getMgo(c echo.Context) *mgo.Database {
	return c.Get("mgok").(*mgo.Database)
}

func mgoMid(db *mgo.Database) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return echo.HandlerFunc(func(c echo.Context) error {
			s := db.Session.Clone()
			defer s.Close()
			c.Set("mgok", s.DB(db.Name))
			return next(c)
		})
	}
}

func newMgo(url, name string) *mgo.Database {
	s, err := mgo.Dial(url)
	if err != nil {
		panic(fmt.Sprintf("%v - %s", err, url))
	}
	return &mgo.Database{s, name}
}

func main() {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
	db := newMgo(DB_Path, DB_Name)
	defer db.Session.Close()

	e.Use(mgoMid(db))

	e.GET("/", getAll)
	e.GET("/movie/:id", getById)
	e.POST("/", postMovie)

	e.Logger.Fatal(e.Start(Api_Port))
}

func postMovie(c echo.Context) error {
	m := new(Movie)
	if err := c.Bind(m); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}
	db := getMgo(c)
	fmt.Println(db)
	if err := db.C(Movie_Coll).Insert(m); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, m)
}

func getAll(c echo.Context) error {
	db := getMgo(c)
	movies := []MovieResponse{}
	err := db.C(Movie_Coll).Find(nil).All(&movies)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, movies)
}

func getById(c echo.Context) error {
	db := getMgo(c)
	id := c.Param("id")
	fmt.Println(id)
	movie := MovieResponse{}
	err := db.C(Movie_Coll).Find(bson.M{"name": id}).One(&movie)
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}
	return c.JSON(http.StatusOK, movie)
}
